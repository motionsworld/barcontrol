<style media="screen">
    md-toolbar .label {
        color : #333;
    }

    body {
        position: initial !important;
        top: initial !important;
        overflow: initial !important;
    }
    html {
        overflow: initial !important;
    }

    /*body {
        top: 0px !important;
    }*/
    .content-wrapper .content {
        padding: 0px !important;
    }
    .whiteframe-wrapper {
        position: relative;
        margin-bottom: 53px;
        margin-top: -65px;
    }

    .option-sidebar {
        animation-duration: 4s;
        animation-delay: 2s;
    }

    .hint {
        font-size: 12px;
        color : #999 !important;
    }

    .selectdemoSelectHeader {
  /* Please note: All these selectors are only applied to children of elements with the 'selectdemoSelectHeader' class */ }
  .selectdemoSelectHeader .demo-header-searchbox {
    border: none;
    outline: none;
    height: 100%;
    width: 100%;
    padding: 0; }
  .selectdemoSelectHeader .demo-select-header {
    box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.1), 0 0 0 0 rgba(0, 0, 0, 0.14), 0 0 0 0 rgba(0, 0, 0, 0.12);
    padding-left: 10.667px;
    height: 48px;
    cursor: pointer;
    position: relative;
    display: flex;
    align-items: center;
    width: auto; }
  .selectdemoSelectHeader md-content._md {
    max-height: 240px; }
</style>
